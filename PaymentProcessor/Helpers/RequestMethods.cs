﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace PaymentProcessor.Helpers
{
    internal class RequestMethods
    {
        internal async Task<string> Get(string UrlRequest, TimeSpan? Timeout = null)
        {
            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                if (!Timeout.HasValue)
                    Timeout = new TimeSpan?(TimeSpan.FromMinutes(2.0));
                using (HttpClient client = new HttpClient())
                {
                    client.Timeout = Timeout.Value;
                    string stringAsync = await client.GetStringAsync(UrlRequest);
                    return stringAsync;
                }
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }
    }
}
