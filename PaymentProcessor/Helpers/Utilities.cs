﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace PaymentProcessor.Helpers
{
    internal class Utilities
    {
        internal static string Md5(string Value)
        {
            using (MD5CryptoServiceProvider cryptoServiceProvider = new MD5CryptoServiceProvider())
            {
                byte[] bytes = Encoding.ASCII.GetBytes(Value);
                byte[] hash = cryptoServiceProvider.ComputeHash(bytes);
                string empty = string.Empty;
                for (int index = 0; index < hash.Length; ++index)
                    empty += hash[index].ToString("x2").ToLower();
                return empty;
            }
        }

        internal static Dictionary<string, string> StringToDictionary(string ToConvert)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            string str1 = ToConvert;
            char[] chArray1 = new char[1] { '&' };
            foreach (string str2 in str1.Split(chArray1))
            {
                char[] chArray2 = new char[1] { '=' };
                string[] strArray = str2.Split(chArray2);
                dictionary.Add(strArray[0], strArray[1]);
            }
            return dictionary;
        }

        internal static string GetToUnixTimestamp()
        {
            return ((int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, 0)).TotalSeconds).ToString();
        }

        internal static string GetUrlToGatewayAuth(
          string POSUrl,
          string UserName,
          string PublicKey,
          string PrivateKey,
          string OrderId,
          Decimal TotalAmount,
          string CreditCard,
          string ExpirationMonth,
          string ExpirationYear,
          string SecurityCodeCVV,
          string ProcessorId)
        {
            string toUnixTimestamp = GetToUnixTimestamp();
            string TotalAmountStr = string.Format("{0:0.00}", TotalAmount);
            return string.Format("{0}?username={1}&type=auth&key_id={2}&hash={3}&time={4}&amount={5}&orderid={6}&processor_id={7}&ccnumber={8}&ccexp={9}&cvv={10}", POSUrl, UserName, PublicKey, Md5(string.Format("{0}|{1}|{2}|{3}", OrderId, TotalAmountStr, toUnixTimestamp, PrivateKey)), toUnixTimestamp, TotalAmountStr, OrderId, ProcessorId, CreditCard, string.Format("{0}{1}", ExpirationMonth, ExpirationYear.Remove(0, 2)), SecurityCodeCVV);
        }

        internal static string GetUrlToGatewaySale(
          string POSUrl,
          string UserName,
          string PublicKey,
          string PrivateKey,
          string TransactionId,
          Decimal TotalAmount,
          string CreditCard,
          string ProcessorId)
        {
            string toUnixTimestamp = GetToUnixTimestamp();
            string TotalAmountStr = string.Format("{0:0.00}", TotalAmount);
            return string.Format("{0}?username={1}&type=sale&key_id={2}&hash={3}&time={4}&transactionid={5}&amount={6}&processor_id={7}&ccnumber={8}", POSUrl, UserName, PublicKey, Md5(string.Format("|{0}|{1}|{2}", TotalAmountStr, toUnixTimestamp, PrivateKey)), toUnixTimestamp, TransactionId, TotalAmountStr, ProcessorId, CreditCard);
        }
    }
}
