﻿using System.Reflection;

[assembly: AssemblyCompany("PaymentProcessor")]
[assembly: AssemblyConfiguration("Debug")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyInformationalVersion("1.0.0")]
[assembly: AssemblyProduct("PaymentProcessor")]
[assembly: AssemblyTitle("PaymentProcessor")]
[assembly: AssemblyVersion("1.0.0.0")]
