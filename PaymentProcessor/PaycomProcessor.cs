﻿using Newtonsoft.Json;
using PaymentProcessor.Helpers;
using PaymentProcessor.Models;
using System;
using System.Net;
using System.Threading.Tasks;

namespace PaymentProcessor
{
    public class PaycomProcessor
    {
        private readonly RequestMethods requestMethods = new RequestMethods();
        private readonly string POSUrl;
        private readonly string POSUserName;
        private readonly string POSPublicKey;
        private readonly string POSPrivateKey;
        private readonly string POSProcessorId;

        public PaycomProcessor(string POSUrl, string POSUserName, string POSPublicKey, string POSPrivateKey, string POSProcessorId)
        {
            this.POSUrl = POSUrl;
            this.POSUserName = POSUserName;
            this.POSPublicKey = POSPublicKey;
            this.POSPrivateKey = POSPrivateKey;
            this.POSProcessorId = POSProcessorId;
        }

        public async Task<TransactionResponse> Authorization(string OrderId, Decimal TotalAmount, string CreditCardNumber, string ExpirationMonth, string ExpirationYear, string SecurityCodeCVV)
        {
            try
            {
                string response = await this.requestMethods.Get(Utilities.GetUrlToGatewayAuth(this.POSUrl, this.POSUserName, this.POSPublicKey, this.POSPrivateKey, OrderId, TotalAmount, CreditCardNumber, ExpirationMonth, ExpirationYear, SecurityCodeCVV, this.POSProcessorId), new TimeSpan?());
                return this.EvalResponse(response);
            }
            catch (Exception ex)
            {
                return this.GetErrorMessage();
            }
        }

        public async Task<TransactionResponse> Sale(Decimal TotalAmount, string TransactionId, string CreditCardNumber)
        {
            try
            {
                string response = await this.requestMethods.Get(Utilities.GetUrlToGatewaySale(this.POSUrl, this.POSUserName, this.POSPublicKey, this.POSPrivateKey, TransactionId, TotalAmount, CreditCardNumber, this.POSProcessorId), new TimeSpan?());
                return this.EvalResponse(response);
            }
            catch (Exception ex)
            {
                return this.GetErrorMessage();
            }
        }

        protected TransactionResponse EvalResponse(string response)
        {
            if (string.IsNullOrEmpty(response))
                return new TransactionResponse()
                {
                    ResponseCode = HttpStatusCode.InternalServerError.ToString(),
                    ResponseText = "Response is empty"
                };
            response = response.TrimStart('?');
            return JsonConvert.DeserializeObject<TransactionResponse>(JsonConvert.SerializeObject(Utilities.StringToDictionary(response)));
        }

        internal TransactionResponse GetErrorMessage()
        {
            return new TransactionResponse()
            {
                ResponseCode = HttpStatusCode.InternalServerError.ToString(),
                ResponseText = "Internal error in DLL"
            };
        }
    }
}
