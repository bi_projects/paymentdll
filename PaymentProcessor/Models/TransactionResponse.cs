﻿using Newtonsoft.Json;

namespace PaymentProcessor.Models
{
    public class TransactionResponse
    {
        [JsonProperty(PropertyName = "response")]
        public int Response { get; set; }

        [JsonProperty(PropertyName = "responseText")]
        public string ResponseText { get; set; }

        [JsonProperty(PropertyName = "authcode")]
        public string Authcode { get; set; }

        [JsonProperty(PropertyName = "transactionid")]
        public string TransactionId { get; set; }

        [JsonProperty(PropertyName = "avsresponse")]
        public string AVSResponse { get; set; }

        [JsonProperty(PropertyName = "cvvresponse")]
        public string CVVResponse { get; set; }

        [JsonProperty(PropertyName = "orderid")]
        public string OrderID { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string TypeTransaction { get; set; }

        [JsonProperty(PropertyName = "response_code")]
        public string ResponseCode { get; set; }

        [JsonProperty(PropertyName = "username")]
        public string UserName { get; set; }

        [JsonProperty(PropertyName = "time")]
        public string Time { get; set; }

        [JsonProperty(PropertyName = "amount")]
        public string Amount { get; set; }

        [JsonProperty(PropertyName = "purshamount")]
        public string PurshAmount { get; set; }

        [JsonProperty(PropertyName = "hash")]
        public string Hash { get; set; }
    }
}
