﻿using PaymentProcessor;
using PaymentProcessor.Models;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MainWebApp.Controllers
{
    public class HomeController : Controller
    {
        public async Task<ActionResult> Index()
        {
            PaycomProcessor processor = new PaycomProcessor("https://paycom.credomatic.com/PayComBackEndWeb/common/requestPaycomService.go", "lchavesq105", "81789294", "mDTb8x5JhZno9vg9111I3W1b91onxGeV", "INET5827");

            TransactionResponse response = await processor.Authorization("123456789", 100.00M, "4111111111111111", "11", "2020","123");

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}